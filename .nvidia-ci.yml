# Copyright (c) 2021, NVIDIA CORPORATION.  All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include:
  - local: '.common-ci.yml'

default:
  tags:
    - type/docker
    - docker/privileged
    - cnt
    - container-dev
    - os/linux

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: "/certs"
  # Release "devel"-tagged images off the master branch
  # RELEASE_DEVEL_BRANCH: "master"
  RELEASE_DEVEL_TAG: "devel"

stages:
  - build
  - test
  - scan
  - deploy
  - release

# The .scan step forms the base of the image scan operation performed before releasing
# images.
.scan:
  stage: scan
  variables:
    REGISTRY: "${CI_REGISTRY_IMAGE}"
    VERSION: "${CI_COMMIT_SHA}"
    # Define both OUT_IMAGE and OUT_IMAGE_TAG to allow for these to be used when scanning the
    # "local" (tagged) image
    OUT_IMAGE_TAG: "${CI_COMMIT_SHA}-${DIST}"
    OUT_IMAGE: "${IMAGE_NAME}"
  except:
    variables:
    - $CI_COMMIT_MESSAGE =~ /\[skip[ _-]scans?\]/i
    - $SKIP_SCANS
  before_script:
    - apk add --no-cache git
    - apk add --no-cache python3 python3-dev py3-pip py3-wheel libmagic
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    - docker pull "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_SHA}-${DIST}"
    - docker tag "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_SHA}-${DIST}" "${OUT_IMAGE}:${OUT_IMAGE_TAG}"
    - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab-master.nvidia.com/sectooling/scanning/contamer.git
    - pip3 install -r contamer/requirements.txt
  script:
    - cd contamer
    - python3 contamer.py -ls --fail-on-non-os ${CONTAMER_SUPPRESS_VULNS:+--suppress-vulns ${CONTAMER_SUPPRESS_VULNS}} -- "${OUT_IMAGE}:${OUT_IMAGE_TAG}"

scan-ubi8:
  extends:
    - .scan
    - .target-ubi8

# Define the external release steps for NGC and Dockerhub
.release:ngc:
  extends: .release:external
  variables:
    OUT_REGISTRY_USER: "${NGC_REGISTRY_USER}"
    OUT_REGISTRY_TOKEN: "${NGC_REGISTRY_TOKEN}"
    OUT_REGISTRY: "${NGC_REGISTRY}"
    OUT_IMAGE: "${NGC_REGISTRY_IMAGE}"

.release:dockerhub:
  extends: .release:external
  variables:
    OUT_REGISTRY_USER: "${REGISTRY_USER}"
    OUT_REGISTRY_TOKEN: "${REGISTRY_TOKEN}"
    OUT_REGISTRY: "${DOCKERHUB_REGISTRY}"
    OUT_IMAGE: "${REGISTRY_IMAGE}"

release:ngc-ubi8:
  extends:
    - .release:ngc
    - .target-ubi8
  dependencies:
    - deploy:sha-ci-ubi8
    - scan-ubi8

release:dockerhub-ubi8:
  extends:
    - .release:dockerhub
    - .target-ubi8
  dependencies:
    - deploy:sha-ci-ubi8
    - scan-ubi8
